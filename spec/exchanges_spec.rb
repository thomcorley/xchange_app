require 'exchange_service'

describe ExchangeService do 

	context "given an invalid currency" do
		it "reurns the error message" do
			expect {
				ExchangeService.new.currency_change("2017-03-20", 10, "fuunre", "47h98fd")
			}.to raise_error("Invalid currency")
		end
	end

	context "given an amount that is not a number" do
		it "returns the error message" do
			expect {
				ExchangeService.new.currency_change("2017-03-20", "fbuk", "USD", "GBP")
			}.to raise_error("Amount is not a number")
		end
	end

end

describe ExchangeService do

	context "if the echange data source is missing" do
		it "should not return a 200 response" do
			expect {
				url = URI.parse('http://www.ecb.europa.eu/stats/eurofxrefgh/eurofxref-hist-90d.xml')
      			req = Net::HTTP::Get.new(url.to_s)
      			res = Net::HTTP.start(url.host, url.port) { |http| http.request(req) }
      			res.code
			}.not_to (eql "200")
		end
	end

end