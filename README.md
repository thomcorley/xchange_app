README

This app is a web interface for currency exchange. It uses the service currency_change which can be found encapsulated in a Gem here:

https://bitbucket.org/thomcorley/currency_change 

The first time this service is called, it makes a GET request to the European Central Bank to retrieve the up-to-date exchange rates. The HTTP response is stored locally in a file named 'xchange_rates.yml'. Any subsequent query made by the service then uses the data from this file. A cron job, defined in config/schedule.rb, downloads the data to this file once a day.