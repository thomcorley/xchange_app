class ExchangeService

  # This service provides the following global variables:
  # ------------------------------------------------------------------------------
  #   $dates ......... an array of the dates for which exchange data is available
  #   $currencies ..... an array of the currencies available (eg: GBP, USD etc)
  # ------------------------------------------------------------------------------
  #
  # The first time this service is called, it makes a get request to the European Central Bank to
  #  retrieve the up-to-date exchange rates. The HTTP response is stored locally in a file named
  #  'xchange_rates.yml'. Any subsequent query made by the service then uses the data from this file.
  #  A cron job, defined in config/schedule.rb, downloads the data to file once a day.
  #

  require 'net/http'
  require 'yaml'
  def initialize
  end

  def currency_change(input_date = Date.today.to_s, amount, curr_from, curr_to)
    
    # Check if exchanges data has been saved to file already. If not, call get_exchange_data to
    #  download the data and save it to file
    if File.exists?('xchange_rates.yml')
      data_hash = yaml_file_to_hash('xchange_rates.yml')
    else
      data_hash = get_exchange_data
    end

    # Create arrays for storing the global dates and currencies
    $dates = []
    $currencies = []
    hash_of_rates = Hash.new 

    # Populate the global dates array
    data_hash.each do |element|
      $dates << element['time']
    end

    from_rate = nil
    to_rate = nil
    curr_from.upcase!
    curr_to.upcase!

    data_hash.each do |a|
      $dates << a['time']
      if a['time'] == input_date
        # This block extracts the rates we want for the given date
        a['Cube'].each do |hash|
          # Populate the rates Hash
          hash_of_rates[hash["currency"]] = hash["rate"]
          # Populate the global currencies
          $currencies << hash['currency']

          from_rate = hash['rate'].to_f if hash['currency'] == curr_from
          to_rate = hash['rate'].to_f if hash['currency'] == curr_to
        end
      end
    end
  
    unless $dates.include? input_date
      data_hash[0]['Cube'].each do |hash|
        $currencies << hash['currency']
        from_rate = hash['rate'].to_f if hash['currency'] == curr_from
        to_rate = hash['rate'].to_f if hash['currency'] == curr_to
      end
    end


    # Including the Euro, which is the base rate
    from_rate = 1 if curr_from == 'EUR'
       to_rate = 1 if curr_to == 'EUR'
    $currencies << 'EUR'

    # Check if the given currencies are valid
    raise "Invalid currency" unless $currencies.include?(curr_from) && $currencies.include?(curr_to)

  

    result = (amount.to_f/from_rate)*to_rate
    result = sprintf('%.2f', result)

    final_result = Result.new(result, hash_of_rates)
  end

  def yaml_file_to_hash(file)
    data = File.read(file)
    Psych.load(data)
  end

  # GET request to the exchanges data URI, save it in cache
  def get_exchange_data
      url = URI.parse('http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml')
      req = Net::HTTP::Get.new(url.to_s)
      res = Net::HTTP.start(url.host, url.port) { |http|
        http.request(req)
      }
      data = res.body
      hash = Hash.from_xml(data)
      data_yaml = hash['Envelope']['Cube']['Cube'].to_yaml
      File.new('xchange_rates.yml', 'w+')
      File.open('xchange_rates.yml', 'w') { |file| file.write(data_yaml)}
      data_hash = yaml_file_to_hash('xchange_rates.yml')
  end
end