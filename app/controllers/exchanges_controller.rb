class ExchangesController < ApplicationController
  require 'exchange_service'
  require 'yaml'
  include ActionView::Helpers::NumberHelper

  def index
    @amount_error = session[:not_a_number]
    ExchangeService.new.currency_change(1, 'USD', 'USD')
    reset_session
  end

  def create
    # Load the names of the currencies from local file
    names_path = Rails.root.join("config", "currency_names.yml").to_s
    @currency_names = YAML.load_file(names_path)

    @date = params[:exchanges]['date']

    amount = params[:exchanges]['amount']
    @amount = sprintf('%.2f', amount.to_f)

    @from_curr = params[:exchanges]['from_curr']
    @to_curr = params[:exchanges]['to_curr']

    result = ExchangeService.new.currency_change(
        @date,
        @amount,
        @from_curr,
        @to_curr)  
    @value = result.value
    @rates = result.rates_hash

    # Check if the amount given is a number, 
    # if not, redirect back to index page with error message.
    # Otherwise, proceed as normal
    if @amount.to_f == 0.0
      not_a_number = "Amount must be a number"
      session[:not_a_number] = not_a_number
      redirect_to :index 
    else 
      render :show
    end
  end

  def show
  end

end




