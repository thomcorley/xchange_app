class ExchangeQuery < ActiveModel
  include ActiveModel::Validations
  attr_accessor :date, :amount, :from_curr, :to_curr, :result

  validates :amount, numericality: true
  validates :amount, presence: true
end