class Result
	def initialize(value, rates_hash)
		@value = value
		@rates_hash = rates_hash
	end

	attr_reader :value, :rates_hash
end
